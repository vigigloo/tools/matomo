resource "random_string" "salt" {
  length  = 32
  special = false
  lifecycle {
    ignore_changes = all
  }
}

resource "helm_release" "matomo" {
  chart           = "matomo"
  repository      = "https://gitlab.com/api/v4/projects/33948292/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/matomo.yaml"),
  ], var.values)

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.nginx_limits_cpu == null ? [] : [var.nginx_limits_cpu]
    content {
      name  = "nginx.resources.limits.cpu"
      value = var.nginx_limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.nginx_limits_memory == null ? [] : [var.nginx_limits_memory]
    content {
      name  = "nginx.resources.limits.memory"
      value = var.nginx_limits_memory
    }
  }

  dynamic "set" {
    for_each = var.nginx_requests_cpu == null ? [] : [var.nginx_requests_cpu]
    content {
      name  = "nginx.resources.requests.cpu"
      value = var.nginx_requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.nginx_requests_memory == null ? [] : [var.nginx_requests_memory]
    content {
      name  = "nginx.resources.requests.memory"
      value = var.nginx_requests_memory
    }
  }

  set {
    name  = "salt"
    value = random_string.salt.result
  }

  dynamic "set" {
    for_each = var.matomo_database_host == null ? [] : [var.matomo_database_host]
    content {
      name  = "database.host"
      value = var.matomo_database_host
    }
  }

  dynamic "set" {
    for_each = var.matomo_database_tables_prefix == null ? [] : [var.matomo_database_tables_prefix]
    content {
      name  = "database.tables_prefix"
      value = var.matomo_database_tables_prefix
    }
  }

  dynamic "set" {
    for_each = var.matomo_database_dbname == null ? [] : [var.matomo_database_dbname]
    content {
      name  = "database.dbname"
      value = var.matomo_database_dbname
    }
  }

  dynamic "set" {
    for_each = var.matomo_database_username == null ? [] : [var.matomo_database_username]
    content {
      name  = "database.username"
      value = var.matomo_database_username
    }
  }

  dynamic "set" {
    for_each = var.matomo_database_password == null ? [] : [var.matomo_database_password]
    content {
      name  = "database.password"
      value = var.matomo_database_password
    }
  }
}
