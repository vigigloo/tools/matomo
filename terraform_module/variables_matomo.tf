variable "matomo_database_host" {
  type = string
}

variable "matomo_database_tables_prefix" {
  type    = string
  default = "matomo_"
}

variable "matomo_database_dbname" {
  type    = string
  default = "matomo"
}

variable "matomo_database_username" {
  type    = string
  default = "matomo"
}

variable "matomo_database_password" {
  type = string
}
