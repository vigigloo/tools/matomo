variable "namespace" {
  type = string
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type    = string
  default = "1.5.1"
}

variable "values" {
  type    = list(string)
  default = []
}

variable "image_repository" {
  type    = string
  default = "matomo"
}

variable "image_tag" {
  type    = string
  default = "4.7.1-fpm-alpine"
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "limits_cpu" {
  type    = string
  default = "100m"
}

variable "limits_memory" {
  type    = string
  default = "128Mi"
}

variable "requests_cpu" {
  type    = string
  default = "100m"
}

variable "requests_memory" {
  type    = string
  default = "128Mi"
}
