variable "nginx_limits_cpu" {
  type    = string
  default = "100m"
}

variable "nginx_limits_memory" {
  type    = string
  default = "128Mi"
}

variable "nginx_requests_cpu" {
  type    = string
  default = "100m"
}

variable "nginx_requests_memory" {
  type    = string
  default = "128Mi"
}
