# Matomo

## Upgrading

The upgrade requires manual steps. It is not enough to just update the version of the docker image.

You'll also need to go to the admin UI and run the automatic update procedure. Sometimes, a database upgrade is also required which can be launched from the web UI.

It's probably better to match the version of the deployed docker image with the version that will be updated to as shown in the UI.
